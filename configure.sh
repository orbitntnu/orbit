#!/bin/bash

# Set the config symlink in the kernel
cd kernel/kernel/config
rm -f live
ln -i -s tms570 live

# Set the config symlink in the fatfs module
cd ../../../kmod/fatfs/config
rm -f live
ln -i -s tms570/normal live

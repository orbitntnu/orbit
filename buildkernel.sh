#!/bin/bash

# Abort immediately if a command fails
set -e

# Function for building some program
function builditem {
	echo "Building" $1
	cd $1
	make clean
	make
	cd ../..
}

# Build elfcheck needed by the build system
builditem util/elfcheck

# Set the config symlink in the kernel
cd kernel/kernel/config
rm -f live
ln -i -s tms570 live
cd ../../..

# Set the config symlink in the fatfs module
cd kmod/fatfs/config
rm -f live
ln -i -s tms570/normal live
cd ../../..

# Build a fresh fatfs kernel module and then the kernel
builditem kmod/fatfs
builditem kernel/kernel

# Print the message
echo \
"Build success! You can now flash kernel/kernel/build/flashimage.bin to" \
"the microcontroller by typing make flash in the kernel/kernel directory."

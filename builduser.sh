#!/bin/bash

# Abort immediately if a command fails
set -e

# Function for building some program
function builditem {
	echo "Building" $1
	cd $1
	make clean
	make
	cd ../..
}

# Build elfcheck needed by the build system, and transfer for uploading
builditem util/elfcheck
builditem util/transfer

# Build the libraries
builditem lib/libcglue
builditem lib/libuser
builditem lib/libsensor

# Build the things in bin
binitems=($(ls -d bin/*))

for i in "${binitems[@]}"
do
	builditem $i
done

# Build the things in usr
usritems=($(ls -d usr/*))

for i in "${usritems[@]}"
do
	builditem $i
done

# Print the message
echo "Build success!"

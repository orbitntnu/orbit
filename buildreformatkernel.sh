#!/bin/bash

# Abort immediately if a command fails
set -e

# Function for building some program
function builditem {
	echo "Building" $1
	cd $1
	make clean
	make $2
	cd ../..
}

# Build elfcheck needed by the build system
builditem util/elfcheck

# Set the config symlink in the kernel
cd kernel/kernel/config
rm -f live
ln -i -s tms570 live
cd ../../..

# Set the config symlink in the fatfs module
cd kmod/fatfs/config
rm -f live
ln -i -s tms570/reformat live
cd ../../..

# Build the C library and util library
builditem lib/libcglue
builditem lib/libuser

# Build the shell and save programs, fatfs kernel module and then the kernel
builditem bin/sh
builditem bin/save
builditem kmod/fatfs mkpreload
builditem kernel/kernel

# Print the message
echo \
"Build success! You can now flash kernel/kernel/build/flashimage.bin to" \
"the microcontroller by typing make flash in the kernel/kernel directory." \
"This will reformat the external file system on the development kit and" \
"load in a shell and save program. It is very important that you then build" \
"a normal kernel and flash it immediately afterwards, use buildkernel.sh"

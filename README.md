# Readme #

## About ##

This is the main repository. For instructions on how to get started, see the
documentation at [the documentation subdomain](https://orbit.erlendjs.no).

## Cloning ##

This repository serves as a main repository linking the individual project
repositories using git submodules. In order to clone it all at once, run

```bash
git clone --recurse-submodules git@git.erlendjs.no:orbit/orbit.git
```

## Building ##

In order to build you must have the toolchain (compilers etc.) installed on
your machine, see the About section above.

The root directory of this repository contains four scripts that will aid
you in the initial build process. Which script to use depends on what kind
of development you are going to do. These are described in their own
subsection, choose one:

### Building a reformat kernel ###

If you have a "blank" development board with an empty external memory chip,
or its file system has been damaged, you will need a "reformat kernel". This is
a special version of the kernel that only reformats the external memory and
loads it with the minimum of tools: the shell (sh) and the save program used
to transfer more programs (save). Use the `buildreformatkernel.sh` script to
build it and then flash the image by typing `make flash` in the directory
`kernel/kernel`.

```bash
./buildreformatkernel.sh
cd kernel/kernel
make flash
cd ../..
```

### Building a normal kernel ###

If you are going to develop kernel code or kernel modules you will need to
configure and build it. The script `buildkernel.sh` initializes some symlinks
for you and builds the kernel:

```bash
./buildkernel.sh
```

You can now make changes to the code and flash the microcontroller by using
the commands `make` and `make flash`. For kernel documentation and
instructions on how to code properly, see [the kernel documentation pages](https://kernel.erlendjs.no).

### Preparing for and building normal programs ###

If you are going to develop a "normal" program, you do not have to build the
kernel or any kernel modules. However, the kernel has to be configured so that
the build system can get the correct compiler flags for the CPU. Configure the
kernel by running the `configure.sh` script. The next step is to build the
utilities, libraries and programs you need. This is all done by the
`builduser.sh` script. Once it has done its job you may transfer the programs
you want to the board by entering their directories and running `make flash`.
Remember to disconnect from the serial terminal first since the transfer
program will need exclusive access to it.

```bash
./configure.sh
./builduser.sh
```

From now on, you only need to run `make` and `make flash` in the repository
of the program you are working on to build and transfer it. You do not have to
go through the above sequence every time you update something. Of course, if
you decide to update all of the user programs you may use the scripts to build
them all at once.

## Rules, Guidelines etc ##

For further information on how to manage repositories, code, ownership,
licencing etc., see the documentation pages linked in the About section at the
top of this page. Individual projects and/or programs may have their own
documentation, check the appropriate repositories for those.
